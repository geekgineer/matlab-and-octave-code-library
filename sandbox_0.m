%clear;clc;close all;
%CHANNEL:CH1
%CLOCK=200uS
%SIZE=130048
%UNITS:V

clear;
clf;
pkg load signal

figure(1);
oscopeFile_1 = dlmread('SomeSavedSignal/signal_5.txt'); 
step = 1%0.05; %50ms = 0.05 sec 
y = oscopeFile_1;
x = 0:step:(length(y) - 1)*step;

plot(x,y,"r","LineWidth",1);

xlabel("time us");
ylabel("Voltage");
%set(gca,'XTick',0::10)

grid on;
zoom xon;

%figure(2);
%nonoise = filtfilt(ones(1,skipstep )/skipstep,1,y);
%plot(x, nonoise,"b","LineWidth",3);
%grid on;
%zoom xon;
%figure(3);
%fftresult=fft(nonoise);
%plot(fftresult);
%specgram(nonoise);

